/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lexico;

import controlador.Utilidades;
import java.util.ArrayList;
import modelo.Tokens;

/**
 *
 * @author alex
 */
public class Lexico {

    private Integer posicion = 0;
    private Integer estado = 0;
    private ArrayList<String> tokens = new ArrayList<>();
    private ArrayList<String> lexemas = new ArrayList<>();
    private String lexema = "";
    private String entrada;
    private Character caracter;
    Utilidades u = new Utilidades();
    
    public Lexico(String entrada) {
        this.entrada = entrada;
    } 
    
    public void imprimir() {
        System.out.println("LITO...................");
        for (int i = 0; i < tokens.size(); i++) {
            System.out.println("TOKEN: "+tokens.get(i)+" LEXEMA: "+lexemas.get(i));
        }
    }
    
    private static int isLetterOrDigit(char c) {
        if ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z')) {
            return 1;
        }else if((c >= '0' && c <= '9')){
            return 0;
        }else{
            return -1;
        }
    }
    
    public void lexico() {
        caracter = entrada.charAt(posicion);
        
        switch(estado) {
            case 0:
                if (u.verificarChar(u.operadoresAritmeticos(), String.valueOf(caracter))) {
                    if (caracter == '+') {
                        lexema = Character.toString(caracter);
                        estado = 0;
                        cargarLexema(lexema, Tokens.SUMA.toString());
                    } else if(caracter == '-'){
                        lexema = Character.toString(caracter);
                        estado = 0;
                        cargarLexema(lexema, Tokens.RESTA.toString());
                    }
                } else if (u.verificarChar(u.operadorAsignar(), String.valueOf(caracter))) {
                    lexema = Character.toString(caracter);
                    estado = 3;
                    cargarLexema(lexema, Tokens.ASIGNAR.toString());
                } else if (this.isLetterOrDigit(caracter) == 0) {
                    estado = 4;
                    lexema = Character.toString(caracter);
                    //cargarLexema(lexema, Tokens.ENTERO.toString());
                } else if (this.isLetterOrDigit(caracter) == 1) {
                    estado = 6;
                    lexema = Character.toString(caracter);
                    //cargarLexema(lexema, Tokens.LETRAS.toString());
                } else {
                    estado = 0;
                    cargarLexema(Character.toString(caracter), Tokens.ERROR.toString());
                    lexema = "";                   
                }
                break;    
            case 4:
                if (u.verificarChar(u.operadoresAritmeticos(), String.valueOf(caracter))) {
                    cargarLexema(lexema, Tokens.ENTERO.toString());
                    lexema = Character.toString(caracter);
                    estado = 0;
                    if (caracter == '+') {     
                        cargarLexema(lexema, Tokens.SUMA.toString());
                    } else if(caracter == '-'){
                        cargarLexema(lexema, Tokens.RESTA.toString());
                    }
                } else if (u.verificarChar(u.operadorAsignar(), String.valueOf(caracter))) {
                    cargarLexema(lexema, Tokens.ENTERO.toString());
                    lexema = Character.toString(caracter);
                    estado = 0;
                    cargarLexema(lexema, Tokens.ASIGNAR.toString());
                } else if (this.isLetterOrDigit(caracter) == 0) {
                    estado = 4;
                    lexema += Character.toString(caracter);
                } else if (this.isLetterOrDigit(caracter) == 1) {
                    cargarLexema(lexema, Tokens.ENTERO.toString());
                    estado = 6;
                    lexema = Character.toString(caracter);
                    //cargarLexema(lexema, Tokens.LETRAS.toString());
                } else {
                    cargarLexema(lexema, Tokens.ENTERO.toString());
                    estado = 0;
                    cargarLexema(Character.toString(caracter), Tokens.ERROR.toString());
                    lexema = "";                   
                }
                break;  
            case 6:
                if (u.verificarChar(u.operadoresAritmeticos(), String.valueOf(caracter))) {
                    cargarLexema(lexema, Tokens.LETRAS.toString());
                    estado = 0;
                    lexema = Character.toString(caracter);
                    if (caracter == '+') {
                        cargarLexema(lexema, Tokens.SUMA.toString());
                    } else if(caracter == '-'){
                        cargarLexema(lexema, Tokens.RESTA.toString());
                    }
                } else if (u.verificarChar(u.operadorAsignar(), String.valueOf(caracter))) {
                    cargarLexema(lexema, Tokens.LETRAS.toString());
                    lexema = Character.toString(caracter);
                    estado = 0;
                    cargarLexema(lexema, Tokens.ASIGNAR.toString());
                } else if (this.isLetterOrDigit(caracter) == 0) {
                    cargarLexema(lexema, Tokens.LETRAS.toString());
                    estado = 4;
                    lexema = Character.toString(caracter);
                    //cargarLexema(lexema, Tokens.ENTERO.toString());
                } else if (this.isLetterOrDigit(caracter) == 1) {
                    estado = 6;
                    lexema += Character.toString(caracter);
                } else {
                    cargarLexema(lexema, Tokens.LETRAS.toString());
                    estado = 0;
                    cargarLexema(Character.toString(caracter), Tokens.ERROR.toString());
                    lexema = "";                   
                }
                break;
            default: break;                   
        }
        posicion++;
        if (posicion >= entrada.length()) {
            if (estado == 4) {
                cargarLexema(lexema, Tokens.ENTERO.toString());
            }
            if (estado == 6) {
                cargarLexema(lexema, Tokens.LETRAS.toString());
            }
        } else {
            lexico();
        }
    }

    private void cargarLexema(String lexema, String token) {
        lexemas.add(lexema);
        tokens.add(token);
    }
    
    public static void main(String[] args) {
        String cadena = "faa+b334=3-s12lakk-3334++23,4";
        Lexico lexico = new Lexico(cadena);
        lexico.lexico();
        lexico.imprimir();
    }
}
