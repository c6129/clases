/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author alex
 */
public class Utilidades {
    
    private static String CARPETA = "data";
    private static String FILE = CARPETA + File.separator + "lexico.properties";
    
    private Properties properties;
    
    public Utilidades() {
        load();
    }

    private void load() {
        try {
            Properties prop = new Properties();
            InputStream in = new FileInputStream(FILE);
            prop.load(in);
            properties = prop;
            in.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Utilidades.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Utilidades.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public String[] operadoresAritmeticos() {
        return properties.getProperty("aritmeticos").split(",");
    }
    
    public String[] reservadaPrint() {
        return properties.getProperty("imprimir").split(",");
    }
    
    public String[] operadorAsignar() {
        String[] aux = new String[1];
        aux[0] = properties.getProperty("asignar");
        return aux;
    }
    
    public boolean verificarChar(String[] datos, String caracter) {
        return Arrays.asList(datos).contains(caracter);
    }
    
}
