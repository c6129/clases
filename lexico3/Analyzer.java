package src.lexico3;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Analyzer {
    public static List<Token> lexical(String input) {
        List<Token> tokens = new ArrayList<Token>();
        StringTokenizer tokenizer = new StringTokenizer(input);

        while (tokenizer.hasMoreElements()) {
            String word = tokenizer.nextToken();
            boolean matched = false;
            boolean find = false;

            for (Types tokenType : Types.values()) {
                Pattern pattern = Pattern.compile(tokenType.pattern);
                Matcher matcher = pattern.matcher(word);

                if (matcher.matches()) {
                    Token token = new Token();
                    token.setType(tokenType);
                    token.setValue(word);
                    tokens.add(token);
                    matched = true;
                    break;
                }

                if (matcher.find())
                    find = true;

            }

            if (!matched) {
                if (find) {
                    System.out.println(word);
                    Automaton automaton = new Automaton();
                    tokens.addAll(automaton.evaluate(word));
                } else {
                    Token token = new Token();
                    token.setType(null);
                    token.setValue(word);
                    tokens.add(token);
                }

            }
        }
        return tokens;
    }

    public static void main(String[] args) {
        // String input = "ENTERO aux = -34;";
        String input = "ENTERO b = 56;";
        // String input = "ENTERO c = aux * b;";
        // String input = "ENTERO suma = 34 / 45;";
        List<Token> tokenList = lexical(input);
        System.out.println("ENTRADA INICIAL ====> " + input + "\n");

        for (Token token : tokenList) {
            if (token.getType() != null)
                System.out.println(token.getType().toString() + " ====> " +
                        token.getValue());
            else
                System.out.println("ERROR ====> " + token.getValue());
        }
    }

}
