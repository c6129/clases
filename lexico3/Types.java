package src.lexico3;

import java.util.regex.Pattern;

//Tipos de lexemas y tokens
public enum Types {
    NUMERO("^[0-9]*$"),
    OPERADOR_ARITMETICO("[*|/|+|-]"),
    PALABRA_RESERVADA(Pattern.quote("INSERTAR")),
    VARIABLE("^[a-zA-Z0-9_.-]*$"),
    ASIGNACION(Pattern.quote(",")),
    FIN_SENTENCIA(Pattern.quote(";")),
    ERROR("ERROR");

    public final String pattern;

    private Types(String pattern) {
        this.pattern = pattern;
    }
}
