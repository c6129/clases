package src.lexico3;

import java.util.ArrayList;
import java.util.List;

public class Automaton {

    private List<Token> tokenList = new ArrayList<Token>();
    private String lexeme = "";

    private void loadToken(Types type, String value) {
        Token token = new Token();
        token.setType(type);
        token.setValue(value);
        tokenList.add(token);
    }

    private State executeTransition(State currentState, char entry) {
        switch (currentState) {
            case INITIAL: {
                lexeme += entry;
                if (String.valueOf(entry).matches(Types.NUMERO.pattern))
                    return State.Q1;
                else if (String.valueOf(entry).matches(Types.VARIABLE.pattern))
                    return State.Q2;
                else if (String.valueOf(entry).matches(Types.OPERADOR_ARITMETICO.pattern)) {
                    loadToken(Types.OPERADOR_ARITMETICO, lexeme);
                    lexeme = "";
                    return State.INITIAL;
                } else if (String.valueOf(entry).matches(Types.FIN_SENTENCIA.pattern)) {
                    loadToken(Types.FIN_SENTENCIA, lexeme);
                    lexeme = "";
                    return State.INITIAL;
                } else if (String.valueOf(entry).matches(Types.ASIGNACION.pattern)) {
                    loadToken(Types.ASIGNACION, lexeme);
                    lexeme = "";
                    return State.INITIAL;
                } else {
                    if (String.valueOf(entry) == " ")
                        return State.INITIAL;
                    loadToken(Types.ERROR, lexeme);
                    lexeme = "";
                    return State.INITIAL;
                }
            }

            case Q1: {
                if (String.valueOf(entry).matches(Types.NUMERO.pattern)) {
                    lexeme += entry;
                    return State.Q1;
                } else if (String.valueOf(entry).matches(Types.VARIABLE.pattern)) {
                    lexeme += entry;
                    return State.Q2;
                } else if (String.valueOf(entry).matches(Types.OPERADOR_ARITMETICO.pattern)) {
                    loadToken(Types.NUMERO, lexeme);
                    lexeme = String.valueOf(entry);
                    loadToken(Types.OPERADOR_ARITMETICO, lexeme);
                    lexeme = "";
                    return State.INITIAL;
                } else if (String.valueOf(entry).matches(Types.ASIGNACION.pattern)) {
                    loadToken(Types.NUMERO, lexeme);
                    lexeme = String.valueOf(entry);
                    loadToken(Types.ASIGNACION, lexeme);
                    lexeme = "";
                    return State.INITIAL;
                } else if (String.valueOf(entry).matches(Types.FIN_SENTENCIA.pattern)) {
                    loadToken(Types.NUMERO, lexeme);
                    lexeme = String.valueOf(entry);
                    loadToken(Types.FIN_SENTENCIA, lexeme);
                    lexeme = "";
                    return State.INITIAL;
                } else {
                    loadToken(Types.NUMERO, lexeme);
                    lexeme = "";
                    return State.INITIAL;
                }
            }

            case Q2: {
                if (String.valueOf(entry).matches(Types.VARIABLE.pattern)) {
                    lexeme += entry;
                    return State.Q2;
                } else if (String.valueOf(entry).matches(Types.OPERADOR_ARITMETICO.pattern)) {
                    loadToken(Types.VARIABLE, lexeme);
                    lexeme = String.valueOf(entry);
                    loadToken(Types.OPERADOR_ARITMETICO, lexeme);
                    lexeme = "";
                    return State.INITIAL;
                } else if (String.valueOf(entry).matches(Types.ASIGNACION.pattern)) {
                    loadToken(Types.VARIABLE, lexeme);
                    lexeme = String.valueOf(entry);
                    loadToken(Types.ASIGNACION, lexeme);
                    lexeme = "";
                    return State.INITIAL;
                } else if (String.valueOf(entry).matches(Types.FIN_SENTENCIA.pattern)) {
                    loadToken(Types.VARIABLE, lexeme);
                    lexeme = String.valueOf(entry);
                    loadToken(Types.FIN_SENTENCIA, lexeme);
                    lexeme = "";
                    return State.INITIAL;
                } else {
                    loadToken(Types.VARIABLE, lexeme);
                    if (String.valueOf(entry) == " ")
                        return State.INITIAL;
                    lexeme = String.valueOf(entry);
                    loadToken(Types.ERROR, lexeme);
                    lexeme = "";
                    return State.INITIAL;
                }
            }

            default:
                return State.INVALIDATION_STATE;
        }
    }

    public List<Token> evaluate(String str) {
        State state = State.INITIAL;
        str += " ";
        for (char c : str.toCharArray()) {
            state = executeTransition(state, c);
        }
        return tokenList;
    }
}
