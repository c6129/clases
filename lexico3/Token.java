package src.lexico3;

public class Token {
    private String value;
    private Types type;

    public String getValue() {
        return value;
    }

    public Types getType() {
        return type;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public void setType(Types type) {
        this.type = type;
    }
}
