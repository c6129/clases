/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package tokenizer.lexico1;

import java.util.StringTokenizer;

/**
 *
 * @author sebas
 */
public class Ejercicio1ST {
    //2 + 4 + 5 = a  ---> 
    public static String linea;
    private static StringTokenizer tokenizer;
    private static String NUMERO = "NUMERO";
    private static String OPERADOR = "OPERADOR";
    private static String lexema;

    public static String getLinea() {
        return linea;
    }

    public static void setLinea(String linea) {        
        Ejercicio1ST.linea = linea;
        Ejercicio1ST.tokenizer = new StringTokenizer(linea);
    }    
    public static String lexico() {
       // System.out.println("----- "+ lexema + "-----");
        if(tokenizer.hasMoreTokens()) {
            lexema = tokenizer.nextToken().trim();
        } else {
            lexema = null;
            return lexema;
        }
        if(lexema.equalsIgnoreCase("+") || lexema.equalsIgnoreCase("-") || lexema.equalsIgnoreCase("*") || lexema.equalsIgnoreCase("/")) {
            return OPERADOR;
        } else {
            try {
                Integer.parseInt(lexema);
                return NUMERO;
            } catch (Exception e) {
                //lexema = "";
                return "ERROR";
            }
        }
    }
    //+6 positio
    //-6
    public static void main(String[] args) {
        String analizar="2a + 4 + 5 + 6";
        setLinea(analizar);
        String token = lexico();
        do {
            System.out.println("TOKEN "+token + "  " + lexema);
            token = lexico();
        }while (token != null);
    }
}
