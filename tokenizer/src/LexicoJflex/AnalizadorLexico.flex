/* Sección de declaraciones de JFlex */
%%
%public
%class AnalizadorLexico
%{
 
 /* Código personalizado */
 
 // Se agregó una propiedad para verificar si existen tokens pendientes
 private boolean _existenTokens = false;
 
 public boolean existenTokens(){
 return this._existenTokens;
 }
 
%}
 
 /* Al utilizar esta instrucción, se le indica a JFlex que devuelva objetos del tipo TokenPersonalizado */
%type TokenPersonalizado
 
%init{
 /* Código que se ejecutará en el constructor de la clase */
%init}
 
%eof{
 
 /* Código a ejecutar al finalizar el análisis, en este caso cambiaremos el valor de una variable bandera */
 this._existenTokens = false;
 
%eof}
 
/* Inicio de Expresiones regulares */
 
 Digito = [0-9]
 Numero = {Digito} {Digito}*
 Letra = [A-Za-z]
 NombreTabla = {Letra} {Letra}*
 DatoTexto = \"{NombreTabla}\"
 Separador = ","
 AperturaParentesis = "("
 CierreParentesis = ")"
 CierreSentencia = ";"
 Espacio = " "
 SaltoDeLinea = \n|\r|\r\n
 PalabraReservada = "INSERTAR"|"EN"|"ESTO"
 ERROR = "*"|"+"|"-"|"/"|"#"\*
 
/* Finaliza expresiones regulares */
 
%%
/* Finaliza la sección de declaraciones de JFlex */
 
/* Inicia sección de reglas */
 
// Cada regla está formada por una {expresión} espacio {código}
{Numero} {
 TokenPersonalizado t = new TokenPersonalizado(yytext(), "DATO:NUMERO");
 this._existenTokens = true;
 return t;
}

{PalabraReservada} {
 TokenPersonalizado t = new TokenPersonalizado(yytext(), "PALABRA_RESERVADA");
 this._existenTokens = true;
 return t;
}
 
{NombreTabla} {
 TokenPersonalizado t = new TokenPersonalizado(yytext(), "NOMBRE_DE_TABLA");
 this._existenTokens = true;
 return t;
}
 
{DatoTexto} {
 TokenPersonalizado t = new TokenPersonalizado(yytext(), "DATO:CADENA");
 this._existenTokens = true;
 return t;
}

{Separador} {
 TokenPersonalizado t = new TokenPersonalizado(yytext(), "SEPARADOR");
 this._existenTokens = true;
 return t;
}

{AperturaParentesis} {
 TokenPersonalizado t = new TokenPersonalizado(yytext(), "APERTURA_AGRUPADOR");
 this._existenTokens = true;
 return t;
}

{CierreParentesis} {
 TokenPersonalizado t = new TokenPersonalizado(yytext(), "CIERRE_AGRUPADOR");
 this._existenTokens = true;
 return t;
}

{CierreSentencia} {
 TokenPersonalizado t = new TokenPersonalizado(yytext(), "CIERRE_SENTENCIA");
 this._existenTokens = true;
 return t;
}
 
{Espacio} {
 // Ignorar cuando se ingrese un espacio
}
 
{SaltoDeLinea} {
 TokenPersonalizado t = new TokenPersonalizado("Enter", "NUEVA_LINEA");
 this._existenTokens = true;
 return t;
}

{ERROR} {
 TokenPersonalizado t = new TokenPersonalizado("Enter", "ERROR");
 this._existenTokens = true;
 return t;
}